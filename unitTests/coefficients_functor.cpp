#include "array.h"
#include "coefficients_functor.h"
#include "gtest/gtest.h"

namespace {
TEST(CoefficientsTest, All) {
  auto functor = CoefficientsFunctor<double>();
  auto table = Array<Array<double>>(2);
  auto arr= Array<double>(2);

  for(int i = 0; i < table.get_size(); i++) {
    table[i] = Array<double>(2);
  }


  table[0][0] = 2;
  table[1][0] = 1;

  arr[0] = 1;
  arr[1] = 1;

  auto result = functor(table, arr);

  EXPECT_FLOAT_EQ(result[0], 1);
  EXPECT_FLOAT_EQ(result[1], 1);
}

}
