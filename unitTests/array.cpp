#include "array.h"
#include "gtest/gtest.h"

namespace {
TEST(ArrayTest, Constructors) {
  //Default Constructor
  EXPECT_NO_THROW(Array<int>());

  //Constructor with size argument
  EXPECT_NO_THROW(Array<float>(25));
  Array<int> testArray;
  Array<int> testArray2(5);
  for(int i = 0; i<5; i++) {
    testArray2[i] = i;
  }

  //Move Constructor
  EXPECT_NO_THROW(testArray = Array<int>());

  //Copy Constructor
  EXPECT_NO_THROW(testArray = testArray2);
  
}

TEST(ArrayTest, Other) {
  Array<int> test(1);
  EXPECT_NO_THROW(test.set_size(5));
  EXPECT_EQ(test.get_size(), 5);
  test[4] = 23;
  EXPECT_EQ(test[4], 23);
}

TEST(ArrayTest, Swap) {
  auto test1 = Array<int>(5);
  auto test2 = Array<int>(5);
  EXPECT_NO_THROW(swap(test1, test2));
}

TEST(ArrayTest, GetAndSet) {
  auto test = Array<int>();
  test.set_size(5);
  EXPECT_EQ(test.get_size(), 5);
}
}
