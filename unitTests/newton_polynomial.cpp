#include "array.h"
#include "newton_polynomial.h"
#include "gtest/gtest.h"

namespace {
TEST(NewtonPolynomialTest, Constructor) {
  //3x^2 + 2x + 1
  auto arr = Array<double>(3);;
  arr[2] = 3;
  arr[1] = 2;
  arr[0] = 1;
  auto functor = NewtonPolynomial<double>(arr);
  EXPECT_FLOAT_EQ(1, functor(0));
  EXPECT_FLOAT_EQ(6, functor(1));
  EXPECT_FLOAT_EQ(17, functor(2));
}

}
