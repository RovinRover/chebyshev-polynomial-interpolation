# SYNOPSIS:
#
#   make all    - makes everything.
#   make driver - makes the driver
#   make test   - makes test files
#   make TARGET - makes the given target.
#   make clean  - removes all executables and objects 

CXX = /usr/bin/g++
CPPFLAGS += -isystem $(GTEST_DIR)/include
CXXFLAGS = -g -Wall -W -pedantic-errors -std=c++17 -pthread

#Directories
SRC_DIR = src
OBJ_DIR = objects
TEST_DIR = unitTests

#Google Test Directory https://github.com/google/googletest
GTEST_DIR = googletest/googletest
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
        		$(GTEST_DIR)/include/gtest/internal/*.h
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

#Get all folders in directories
SRC_STRUCT := $(shell find $(SRC_DIR) -type d)
TEST_STRUCT := $(shell find $(TEST_DIR) -type d)

#Get all files in directories
SRC_FILES := $(addsuffix /*, $(SRC_STRUCT))
SRC_FILES := $(wildcard $(SRC_FILES))

TEST_FILES := $(addsuffix /*, $(TEST_STRUCT))
TEST_FILES := $(wildcard $(TEST_FILES))

#Entry Point
MAIN = $(addprefix $(SRC_DIR)/, main.cpp)

#Filter files
CPP_FILES := $(filter %.cpp,$(SRC_FILES))
CPP_FILES := $(filter-out $(MAIN), $(CPP_FILES))
HDR_FILES := $(filter %.h, $(SRC_FILES))
HPP_FILES := $(filter %.hpp, $(SRC_FILES))

#Object files
CPP_OBJS := $(CPP_FILES:%.cpp=%.o)
CPP_OBJS := $(notdir $(CPP_OBJS))
CPP_OBJS := $(addprefix $(OBJ_DIR)/, $(CPP_OBJS))

TEST_OBJS := $(TEST_FILES:%.cpp=%_test.o)
TEST_OBJS := $(notdir $(TEST_OBJS))
TEST_OBJS := $(addprefix $(OBJ_DIR)/, $(TEST_OBJS))

#Google-test recipes from example makefile
$(addprefix $(OBJ_DIR)/, gtest-all.o) : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
	$(GTEST_DIR)/src/gtest-all.cc -o $@

$(addprefix $(OBJ_DIR)/, gtest_main.o) : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
    $(GTEST_DIR)/src/gtest_main.cc -o $@

$(addprefix $(OBJ_DIR)/, gtest.a) : $(addprefix $(OBJ_DIR)/, gtest-all.o)
	$(AR) $(ARFLAGS) $@ $^

$(addprefix $(OBJ_DIR)/, gtest_main.a) : $(addprefix $(OBJ_DIR)/, gtest-all.o) \
										 $(addprefix $(OBJ_DIR)/, gtest_main.o)
	$(AR) $(ARFLAGS) $@ $^

#Recipe for objects from source directory
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

#Recipe for test objects
$(OBJ_DIR)/%_test.o: $(TEST_DIR)/%.cpp $(GTEST_HEADERS) $(HDR_FILES)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -I$(SRC_DIR) -c $< -o $@

.DEFAULT_GOAL := driver

all: driver test

driver: $(CPP_OBJS) $(MAIN)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(CPP_OBJS) $(MAIN) -o $@

test: $(TEST_OBJS) $(CPP_OBJS) $(addprefix $(OBJ_DIR)/, gtest_main.a) $(HDR_FILES) $(HPP_FILES)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(TEST_OBJS) $(CPP_OBJS) $(addprefix $(OBJ_DIR)/, gtest_main.a) -o $@

debug:
	-@echo $(HDR_FILES)

clean:
	-@rm -f core
	-@rm -f driver
	-@rm -f test
	-@rm -f objects/*
