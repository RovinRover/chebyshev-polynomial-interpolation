#include <cmath>
#include <iostream>

template <class T>
Array<T> ChebyshevRoots<T>::operator()(const int num_of_roots) {
  Array<T> roots = Array<T>(num_of_roots);
  for(int i = 1; i <= num_of_roots; i++) {
    roots[num_of_roots-i] = std::cos( (2*i-1) * M_PI / (2*num_of_roots));
  }
  return roots;
}
