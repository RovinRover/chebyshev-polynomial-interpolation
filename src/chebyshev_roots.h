#ifndef CHEVYSHEV_ROOTS_H
#define CHEVYSHEV_ROOTS_H

#include "array.h"

/**
 * \class ChebyshevRoots
 * \brief Functor for finding chebyshev roots
 * \tparam T must have the following operators overloaded:
 *  -, *, /
 */
template <class T>
class ChebyshevRoots {
 public:
  /**
   * \brief ChebyshevRoots functor
   * \pre num_of_roots should be positive and nonzero
   * \post An array of chebyshev roots are returned in ascending order
   */
  Array<T> operator()(const int num_of_roots);
};

#include "chebyshev_roots.hpp"
#endif  // CHEVYSHEV_ROOTS_H
