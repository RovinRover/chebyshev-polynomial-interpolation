template <class T>
Array<T> CoefficientsFunctor<T>::operator()(Array<Array<T>>& table, Array<T>& x_values) {

  Array<T> newton_coefficients = Array<T>(table.get_size());
  Array<T> return_values = Array<T>(table.get_size());
  Array<Array<T>> intermediates = Array<Array<T>>(table.get_size());

  for(int i = 0; i < return_values.get_size(); i++) {
    return_values[i] = 0;
  }

  for(int i = 0; i < newton_coefficients.get_size(); i++) {
    newton_coefficients[i] = table[i][0];
  }

  intermediates[0] = Array<T>(1);
  intermediates[0][0] = 1;
  for(int i = 1; i < intermediates.get_size(); i++) {
    intermediates[i] = Array<T>(i + 1);
    for(int j = 0; j < i + 1; j++) {
      intermediates[i][j] = 0;
      if(j != 0) {
        intermediates[i][j] += intermediates[i-1][j-1];
      }
      if(j != i) {
        intermediates[i][j] -= x_values[i-1]*intermediates[i-1][j]; 
      }
    }
  }

  for(int i = 0; i < intermediates.get_size(); i++) {
    for(int j = i; j < intermediates.get_size(); j++) {
      return_values[i] += intermediates[j][i] * newton_coefficients[j];
    }
  }

  return return_values;
}
