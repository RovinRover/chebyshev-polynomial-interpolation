#ifndef NEWTON_POLYNOMIAL_H
#define NEWTON_POLYNOMIAL_H

#include "array.h"
/**
 * \class NewtonPolynomial
 * \brief Functor for polynomial values at certain points
 * \tparam T must have the following operators overloaded:
 *  +=, -, * and be able to be constructed from a single int
 */
template <class T>
class NewtonPolynomial {
 public:
  /**
   * \brief Constructor
   * \pre Coefficients should be in ascending order from x^0 to x^n
   * \post Functor with coefficients is created
   */
  NewtonPolynomial(Array<T> coefficients);

  /**
   * \brief Evaluates the polynomial at x_value
   * \pre None
   * \post Newton interpolation value is returned
   */
  T operator()(T x_value);
 private:
  Array<T> coefficients_;
  Array<T> x_values_;
};


#include "newton_polynomial.hpp"
#endif  // NEWTON_POLYNOMIAL_H
