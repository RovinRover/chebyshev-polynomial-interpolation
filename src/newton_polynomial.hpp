#include <cmath>

template <class T>
NewtonPolynomial<T>::NewtonPolynomial(Array<T> coefficients) {
  coefficients_ = Array<T>();
  swap(coefficients_, coefficients);
}

template <class T>
T NewtonPolynomial<T>::operator()(T x_value) {
  T return_value = 0;
  for(int i = 0; i < coefficients_.get_size(); i++) {
    return_value += coefficients_[i] * std::pow(x_value, i);
  }
  return return_value;
}
