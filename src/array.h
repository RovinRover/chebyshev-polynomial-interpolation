#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>

template <class T>
class Array;

template <class T>
void swap(Array<T>& first, Array<T>& second);

template <class T>
std::ostream& operator<<(std::ostream& os, const Array<T>& array);

template <class T>
std::istream& operator>>(std::ostream& is, Array<T>& array);

/**
 * \class Array
 * \brief Basic array class
 * \tparam T No requirements
 */
template <class T>
class Array {
 public:
  /**
   * \brief Default construtor
   * \pre None
   * \post An empty array is constructed
   */
  Array();

  /**
   * \brief Constructor
   * \pre Size must be greater than 0
   * \post An array of the size specified is constructed
   */
  Array(const int& size);

  /**
   * \brief Copy constructor
   * \pre None
   * \post A deep copy of the array passed will be constructed.
   */
  Array(const Array& source);

  /**
   * \brief Move constructor
   * \pre None
   * \post An array will be move constructed
   */
  Array(Array&& source);

  /**
   * \brief Destructor
   * \pre None
   * \post Array will be destroyed and memory will be freed
   */
  ~Array();

  /**
   * \brief Resizes array to size specified
   * \pre None
   * \post Array will be of size specified
   * \note Data in array before resizing will be lost
   */
  void set_size(const int& size);

  /**
   * \brief Returns size of array
   * \pre None
   * \post Size is returned
   */
  int get_size() const {return size_;}

  /**
   * \brief Assignment operator
   * \pre None
   * \post Calling object will be a copy of source
   * \note Uses either move constructor or copy constructor
   */
  Array<T>& operator=(Array source);

  /**
   * \brief Bracket operator
   * \param i should be >= 0 and < size of the array
   * \pre Must have valid i
   * \post Value at index i is returned
   * \note Throws out_of_range error on invalid i
   */
  T& operator[](const int& i);

  /**
   * \brief Bracket operator
   * \param i should be >= 0 and < size of the array
   * \pre Must have valid i
   * \post Value at index i is returned
   * \note Throws out_of_range error on invalid i
   */
  const T& operator[](const int& i) const;

  /**
   * \brief Insertion operator
   * \pre None
   * \post Output the array
   */
  friend std::ostream& operator<< <T>(std::ostream& os, const Array<T>& array);

  /**
   * \brief Extraction operator
   * \pre 
   * \post
   */
  friend std::istream& operator>> <T>(std::ostream& is, Array<T>& array);

  /**
   * \brief Swap function
   * \pre None
   * \post Swaps the two arrays
   * \note Does not throw an exception
   */
  friend void swap<>(Array<T>& first, Array<T>& second);
 private:
  T* data_;
  int size_;
};

#include "array.hpp"
#endif  // ARRAY_H
