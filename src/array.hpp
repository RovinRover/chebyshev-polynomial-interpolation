#include <algorithm>
#include <stdexcept>

template <class T>
Array<T>::Array() {
  size_ = 0;
  data_ = nullptr;
}

template <class T>
Array<T>::Array(const int& size) {
  if(size <= 0) {
    throw std::invalid_argument("Array size must be greater than 0");
  }
  size_ = size;
  data_ = new T[size];
}

template <class T>
Array<T>::Array(const Array& source) {
  size_ = source.size_;
  data_ = new T[size_];
  for(int i=0; i<size_; i++) {
    data_[i] = source.data_[i];
  }
}

template <class T>
Array<T>::Array(Array&& source) : Array() {
  swap(*this, source);
}

template <class T>
Array<T>::~Array() {
  delete[] data_;
}

template <class T>
void Array<T>::set_size(const int& size) {
  delete[] data_;
  data_ = new T[size];
  size_ = size;
}

template <class T>
Array<T>& Array<T>::operator=(Array<T> source) {
  swap(*this, source);
  return *this;
}

template <class T>
T& Array<T>::operator[](const int& i) {
  if(i < 0 || i >= size_) {
    throw std::out_of_range("Invalid index");
  }
  return data_[i];
}

template <class T>
const T& Array<T>::operator[](const int& i) const {
  if(i < 0 || i >= size_) {
    throw std::out_of_range("Invalid index");
  }
  return data_[i];
}

template <class T>
void swap(Array<T>& first, Array<T>& second) {
  std::swap(first.size_, second.size_);
  std::swap(first.data_, second.data_);
}

template <class T>
std::ostream& operator<<(std::ostream& os, const Array<T>& array) {
  for(int i = 0; i < array.size_; i++) {
    os << array[i] << std::endl;
  }
  os << std::endl;
  return os;
}

template <class T>
std::istream& operator>>(std::ostream& is, Array<T>& array) {
  for(int i = 0; i < array.size_; i++) {
    is >> array[i];
  }
  return is;
}
