#ifndef COEFFICIENTS_FUNCTOR_H
#define COEFFICIENTS_FUNCTOR_H

#include "array.h"

/**
 * \class CoefficientsFunctor
 * \brief Functor for creating coefficients in standard form polynomial from divided difference table
 * \tparam T must have the following operators overloaded:
 *  +=, -=, * and must be able to be constructed from an int
 */
template <class T>
class CoefficientsFunctor {
 public:
  /**
   * \brief Coefficients functor
   * \pre Table and x_values must be of the same nonzero size
   * \pre Table must have newton coefficients stored at table[i][0] for every i in table
   * \post Coefficients in standard form are returned in ascending order
   */
  Array<T> operator()(Array<Array<T>>& table, Array<T>& x_values);
};

#include "coefficients_functor.hpp"
#endif // COEFFICIENTS_FUNCTOR_H
