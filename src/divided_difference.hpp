#include <exception>

template <class T>
Array<Array<T>> DividedDifference<T>::operator()( 
    const Array<T>& x_values,
    const Array<T>& y_values) const {
  if(x_values.get_size() != y_values.get_size() || x_values.get_size() == 0) {
    throw std::invalid_argument("X and y arrays must be of the same size and nonzero.");
  }
  Array<Array<T>> table = Array<Array<T>>(x_values.get_size());

  //Setup table
  for(int i = 0; i < table.get_size(); i++) {
    table[i].set_size(table.get_size() - i);
  }

  //Initialize first column
  for(int i = 0; i < table.get_size(); i++) {
    table[0][i] = y_values[i];
  }

  //Calculate the rest of the table
  for(int i = 1; i < table.get_size(); i++) {
    for(int j = 0; j < table[i].get_size(); j++) {
      table[i][j] = (table[i-1][j+1] - table[i-1][j]) / (x_values[i+j] - x_values[j]);
    }
  }

  return table;
}
