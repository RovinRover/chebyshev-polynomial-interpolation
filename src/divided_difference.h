#ifndef DIVIDED_DIFFERENCE_H
#define DIVIDED_DIFFERENCE_H

#include "array.h"

/**
 * \class DividedDifference
 * \brief Functor for creating a divided difference table
 * \tparam T must have the following operators overloaded:
 *  =, -, /
 */
template <class T>
class DividedDifference {
 public:
  /**
   * \brief Divided Difference functor
   * \pre x_values and y_values must be of the same nonzero size
   * \pre x_values and y_values should be sorted in ascending order
   * \pre All x_values should be unique
   * \post Divided difference table is returned
   * /note Coefficients are stored in [i][0] where 0 <= i < table.size()
   */
  Array<Array<T>> operator()(
      const Array<T>& x_values,
      const Array<T>& y_values) const;
};


#include "divided_difference.hpp"
#endif  // DIVIDED_DIFFERENCE_H 

