#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <string>
#include "divided_difference.h"
#include "newton_polynomial.h"
#include "chebyshev_roots.h"
#include "coefficients_functor.h"
#include "array.h"

int main(int argc, char** argv){
  const double start_value = 0.1;
  const double step_value = 0.2;
  const int num_of_steps = 5;
  const int sig_figs = 8;

  std::ifstream data_file;
  DividedDifference<double> divided_difference_functor;
  ChebyshevRoots<double> chebyshev_roots_functor;
  CoefficientsFunctor<double> coefficients_functor;
  Array<Array<double>> divided_difference_table;
  Array<double> function_values = Array<double>(num_of_steps);
  Array<double> equal_space_absolute_error = Array<double>(num_of_steps);
  Array<double> chebyshev_absolute_error = Array<double>(num_of_steps);
  Array<double> equal_space_relative_error = Array<double>(num_of_steps);
  Array<double> chebyshev_relative_error = Array<double>(num_of_steps);
  Array<double> equal_space_interpolant = Array<double>(num_of_steps);
  Array<double> chebyshev_interpolant = Array<double>(num_of_steps);

  int num_of_pairs;
  int num_of_roots;
  Array<double> equal_space_x;
  Array<double> equal_space_y;
  Array<double> equal_space_coefficients;
  Array<double> chebyshev_x;
  Array<double> chebyshev_y;
  Array<double> chebyshev_coefficients;

  std::cout.precision(sig_figs);

  //Argument Checking
  if(argc != 3) {
    std::cout<< "Please pass 2 arguments (the number of points) (dataset file name)" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  try {
    num_of_roots = std::stoi(argv[1]);
  } catch(...) {
    std::cout<< "Invalid argument: unable to convert number passed" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  data_file = std::ifstream(argv[2]);

  if(!data_file.good()) {
    std::cout<< "Error opening file." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  if(data_file.peek() == std::ifstream::traits_type::eof()) {
    std::cout<< "File passed is empty" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  //Data Input
  data_file >> num_of_pairs;
  equal_space_x = Array<double>(num_of_pairs);
  equal_space_y = Array<double>(num_of_pairs);
  equal_space_coefficients = Array<double>(num_of_pairs);

  for(int i = 0; i < num_of_pairs; i++) {
    if(data_file.peek() == std::ifstream::traits_type::eof()){
      std::cout << "File appears to be truncated" << std::endl;
      std::exit(EXIT_FAILURE);
    }
    data_file >> equal_space_x[i];
    if(data_file.peek() == std::ifstream::traits_type::eof()){
      std::cout << "File appears to be truncated" << std::endl;
      std::exit(EXIT_FAILURE);
    }
    data_file >> equal_space_y[i];
  }
  
  //Create newton polynomial from evenly spaced values
  divided_difference_table = divided_difference_functor(equal_space_x, equal_space_y);
  equal_space_coefficients = coefficients_functor(divided_difference_table, equal_space_x);
  NewtonPolynomial<double> equal_space_polynomial = NewtonPolynomial<double>(equal_space_coefficients);

  //Create chebyshev data points
  chebyshev_x = chebyshev_roots_functor(num_of_roots);
  chebyshev_y = Array<double>(num_of_roots);
  for(int i = 0; i < chebyshev_x.get_size(); i++) {
    chebyshev_y[i] = 1/(1+12*chebyshev_x[i]*chebyshev_x[i]);
  }

  //Create chebyshev polynomial
  divided_difference_table = divided_difference_functor(chebyshev_x, chebyshev_y);
  chebyshev_coefficients = coefficients_functor(divided_difference_table, chebyshev_x);
  NewtonPolynomial<double> chebyshev_polynomial = NewtonPolynomial<double>(chebyshev_coefficients);

  //Calculate values and errors
  for(int i = 0; i < num_of_steps; i++) {
    double x = start_value + i*step_value;
    equal_space_interpolant[i] = equal_space_polynomial(x);
    chebyshev_interpolant[i] = chebyshev_polynomial(x);
    function_values[i] = 1/(1+12*x*x);
    chebyshev_absolute_error[i] = std::abs(chebyshev_interpolant[i] - function_values[i]);
    chebyshev_relative_error[i] = 100 * chebyshev_absolute_error[i] / function_values[i];
    equal_space_absolute_error[i] = std::abs(equal_space_interpolant[i] - function_values[i]);
    equal_space_relative_error[i] = 100 * equal_space_absolute_error[i] / function_values[i];
  }

  //Output
  std::cout << "1. Chebyshev data points:" << std::endl;
  for(int i = 0; i < chebyshev_x.get_size(); i++) {
    std::cout<< chebyshev_x[i] << " " << chebyshev_y[i] << std::endl;
  }
  std::cout << std::endl;

  std::cout << "2. The computed coefficients making up the chebyshev interpolant:" << std::endl;
  std::cout << chebyshev_coefficients;
  std::cout << std::endl;

  std::cout << "3. Interpolant values:" << std::endl;
  for(int i = 0; i < num_of_steps; i++) {
    std::cout << start_value + i*step_value << " " << chebyshev_interpolant[i] << std::endl;
  }
  std::cout << std::endl;

  std::cout << "4. Function values:" << std::endl;
  for(int i = 0; i < num_of_steps; i++) {
    std::cout << start_value + i*step_value << " " << function_values[i] << std::endl;
  }
  std::cout << std::endl;

  std::cout << "5. Absolute Error:" << std::endl;
  std::cout << chebyshev_absolute_error;
  std::cout << std::endl;

  std::cout << "6. Relative Error:" << std::endl;
  for(int i = 0; i < num_of_steps; i++) {
    std::cout << chebyshev_relative_error[i] << "%" << std::endl;
  }
  std::cout << std::endl;

  std::cout << "7. Difference in relative change between chebyshev and equal spaced interpolant values" << std::endl;
  for(int i = 0; i < num_of_steps; i++) {
    std::cout << equal_space_relative_error[i] - chebyshev_relative_error[i] << "%" << std::endl;
  }
  std::cout << std::endl;
}
